annual_salary = int(input("Enter your starting annual salary:"))
portion_saved = float(input("Enter persent of your salary to save, as a decimal:"))
total_cost = int(input("Enter cost of your dream house:"))
semi_annual_rise = float(input("Enter semi annual rise, as a decimal:"))

r = .04
months = 0
current_savings = 0

portion_down_payment = total_cost * .25
additional = r * current_savings / 12
monthy_salary = annual_salary / 12

while (current_savings < portion_down_payment):
    additional = r * current_savings / 12
    current_savings += additional + monthy_salary*portion_saved
    months+=1
    if (months % 6 == 0):
        monthy_salary += monthy_salary*semi_annual_rise

print(f"Number of month: {months}")