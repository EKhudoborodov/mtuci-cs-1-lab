annual_salary = int(input("Enter the starting salary:"))

total_cost = 1000000
semi_annual_rise = .07
r = .04
period, months = 36, 0


portion_down_payment = total_cost * .25
for i in range (0, 10001, 1):
    current_savings = 0
    months_left = period
    monthy_salary = annual_salary / 12
    while(months_left != 0):
        portion_saved = float(i / 10000)
        additional = r * current_savings / 12
        current_savings += additional + monthy_salary*portion_saved
        months_left -= 1
        months += 1
        if (months % 6 == 0):
            monthy_salary += monthy_salary*semi_annual_rise
    if (current_savings > portion_down_payment):
        print(f"Best savings rate: {portion_saved}")
        break
    else:
        if (i == 10000):
            print("It is not possible to pay the down payment in three years.")