annual_salary = int(input("Enter your starting annual salary:"))
portion_saved = float(input("Enter persent of your salary to save, as a decimal:"))
total_cost = int(input("Enter cost of your dream house:"))
r = .04
months = 0
current_savings = 0

portion_down_payment = .25 * total_cost
additional = r * current_savings / 12
monthly_salary = annual_salary / 12

while (current_savings < portion_down_payment):
    additional = r * current_savings / 12
    current_savings += additional + monthly_salary * portion_saved
    months+=1

print(f"Number of month: {months}")
